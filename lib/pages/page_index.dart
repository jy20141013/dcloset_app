import 'dart:ui';

import 'package:dcloset_app/components/component_item_info.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppBar(
              backgroundColor: Color(0xFFFFFF),
              leading: IconButton(
                icon: Icon(Icons.menu_book),
                onPressed: () {},
              ),
              title: Container(
                height: 60,
                alignment: Alignment.center,
                child: Image.asset('assets/logoIcon.png'),
              ),
              actions: [
                IconButton(
                  onPressed: (){},
                  icon: Icon(Icons.my_library_add),
                )
              ],
            )
          ],
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text(
                  '" 당신만의 놀라운 Daily Closet, 디클로젯 "',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
            Container(
              child: Image.asset(
                  'assets/main.jpg',
              fit: BoxFit.cover,
              width: double.infinity,
                height: 500,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text('DCloset Catalog',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
              ),
            ),
            ),
            Container(
              child: _buildBody(context),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return GridView(
      children: [
        ComponentItemInfo(
            imgUrl: 'assets/catalog1.png',
            goodsName: '키치 코디 세트',
            goodsPrice: 39000,
            callback: () {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => ComponentItemInfo()));
            },
        ),
      ],
    );
  }

}

