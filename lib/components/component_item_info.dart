import 'package:flutter/material.dart';

class ComponentItemInfo extends StatelessWidget {
  const ComponentItemInfo({
    super.key,
    required this.imgUrl,
    required this.goodsName,
    required this.goodsPrice,
    required this.callback
  });

  final String imgUrl;
  final String goodsName;
  final num goodsPrice;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Image.asset(imgUrl),
            Text(goodsName),
            Text('${goodsPrice}원'),
          ],
        ),
      ),
    );
  }
}
